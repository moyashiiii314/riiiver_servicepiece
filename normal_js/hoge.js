const prefectureData = require("../hoge.json");

// 県名リスト
const prefectureList = ["北海道","青森","岩手","宮城","秋田","山形","福島","茨城","栃木","群馬","埼玉","千葉","東京都","神奈川","新潟","富山","石川","福井","山梨","長野","岐阜","静岡","愛知","三重","滋賀","京都","大阪","兵庫","奈良","和歌山","鳥取","島根","岡山","広島","山口","徳島","香川","愛媛","高知","福岡","佐賀","長崎","熊本","大分","宮崎","鹿児島","沖縄"]

// 0 ~ リストの数でランダムに数字を選ぶ
var randomNum = Math.floor( Math.random() * prefectureList.length );

// ランダムに選んだ数字の県名
const prefecture = prefectureList[randomNum];

// 選んだ県名のID
for (var num = 0; num < prefectureList.length ;num ++) {
  
  if (prefecture === prefectureData[num]["name"]) {
    var prefectureID = prefectureData[num]["id"];
  }
}

// request-promiseを読み込む
const requestPromise = require("request-promise");

// 使用するAPIサービスのURLを設定します openweathermapとgas
const openWeatherMapApi = "http://api.openweathermap.org/data/2.5/weather";
const gasApi = "https://script.google.com/macros/s/AKfycbyVqsWHZkMFmvb1JShmTf-BKqomwjqry4Mz9E6R4tKzQwzByBo/exec"
const openWeatherMapApiKey = "f3c00053bff6503205042aef63ad7797";

const result = {};

const getWeatherData = async () => {

  const openWeatherMapUrl = `${openWeatherMapApi}?id=${prefectureID}&appid=${openWeatherMapApiKey}`;

  try {
    const openWeatherMapResult = await requestPromise({
      method: "GET",
      uri: openWeatherMapUrl,
      json: true
    })
    const temp = openWeatherMapResult.main.temp - 273.15;
    const name = openWeatherMapResult.name;
    
    result[name] = temp;
    return result;

  } catch (error) {
    console.log("Error message: " + error.message);
  } 
}

getWeatherData();

const sendGoogleSpreadSheet = async () => {
  const sendData = await getWeatherData();
  console.log(sendData);
  try {
    const gasResult = await requestPromise({
      method: "POST",
      uri: gasApi,
      json: sendData
    })

    return gasResult;
  } catch (error) {
      console.log("Error message: " + error.message);
  };
}

sendGoogleSpreadSheet();