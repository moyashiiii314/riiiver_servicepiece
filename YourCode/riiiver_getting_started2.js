exports.handler = async (event) => {
// 受け取ったデータから県名を設定
const prefectureID = event["properties"]["preferences"]["prefecture"];

// 県名からIDを取得(今までは県名をそのまま追加しましたが、名前の表記が変わっても問題ないようIDにしています)
// const prefectureID = prefectureIDList[prefecture];

// request-promiseを読み込みます
const requestPromise = require("request-promise");

// openweathermapのURLとSpread SheetのURLを設定
const api = "http://api.openweathermap.org/data/2.5/weather";
const gasApi = event.properties.preferences.gasURL;

// 送信するデータを設定 ここではAPIキー
const apiKey = event.properties.preferences.opernWeatherMapAPIKey;

// URL作成 URL + APIキー + 都市ID (ついでに℃になるように設定)
const url = `${api}?APPID=${apiKey}&id=${prefectureID}&units=metric`;

// 以降がAPI通信の処理となります
const weatherData = {}; // 後から必要な情報を追加するために空のデータを作成

const openWeatherMap = async () => {
  try {
    const result = await requestPromise({
      method: "GET",
      uri: url
    });

    let parseResult = JSON.parse(result); // JavaScriptで扱えるようにする(おまじない)

    const temp = parseResult["main"]["temp"]; // 必要な情報を抜き出す
    const weather = parseResult["weather"][0]["main"]; // 必要な情報を抜き出す

    weatherData["temp"] = temp; // 欲しいデータを空のデータに追加 JSON形式
    weatherData["weather"] = weather; // 欲しいデータを空のデータに追加 JSON形式

    return weatherData;

  } catch (error) {
    console.log("Error message: " + error.message);
  }
};

const sendGasData = async () => {
  const sendData = await openWeatherMap();
  console.log(sendData);
  try {
    const gasResult = await requestPromise({
      method: "GET",
      uri: gasApi,
      qs: sendData
  });

    const response = {
      status: 200,
      body: {
        successFlag: true
      }
    }

    return response;

  } catch (error) {
      console.log("Error message: " + error.message);

      const response = {
        status: 400,
        body: {
          successFlag: false
        }
      }
      return response;
    }
  };

  return await sendGasData();
};
